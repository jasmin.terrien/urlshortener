#!/bin/sh
genShortURL () {
	jsonReq='{"url":"'$1'"}'
	respGenShortURL=$(curl -s -d $jsonReq -H "Content-Type: application/json" -X POST http://localhost:8080/ |sed -E 's/.*"url":"?([^,"]*)"?.*/\1/')
	
}
getURL() {
	respGetURL=$(curl -Ls -o /dev/null -w %{url_effective} $1)
}
checkStr(){
	if [ "$1" != "$2" ]; then
		echo "ERROR"
		exit 7
	fi
}

echo "---------Test OK---------"
longUrl="http://www.google.fr/"
genShortURL $longUrl
getURL $respGenShortURL
checkStr $longUrl $respGetURL
echo "SUCCESS"

echo "---------Test OK REPEAT---------"
longUrlNew="http://www.google.fr/"
genShortURL $longUrlNew
getURL $respGenShortURL
checkStr $longUrl $respGetURL
echo "SUCCESS"

echo "---------Test WRONG format URL---------"
longUrl="ftl://www.google.fr/"
genShortURL $longUrl
checkStr $respGenShortURL "Invalid_URLShortener"
echo "SUCCESS"

echo "---------Test WRONG ID---------"
wrongID="http://localhost:8080/r/a"
getURL $wrongID
checkStr $respGetURL "http://localhost:8080/notfound"
echo "SUCCESS"

echo "---------Test too long ID---------"
fakeID="http://localhost:8080/r/1234567891011"
getURL $fakeID
checkStr $respGetURL "http://localhost:8080/teapot"
echo "SUCCESS"


echo "---------Test 100 generation and redirect---------"

input="urlListExample.txt"
start=`date +%s`
while IFS= read -r line; do
	genShortURL $line
	getURL $respGenShortURL
	checkStr $line $respGetURL
done < "$input"
end=`date +%s`
duration=$((end-start))
echo "SUCCESS in $duration s"