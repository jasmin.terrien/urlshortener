package com.jtn.demo.urlshortener;

import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.headers()
                .contentSecurityPolicy("script-src 'self' 'nonce-pmV4c3BhcmtlciDydWxlcyA7VQ==' 'nonce-zx82zafucAzx82zafucA=='");
        http.csrf().disable(); //TODO : enable when HTTPS without it's useless
    }
}
