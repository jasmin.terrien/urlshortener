package com.jtn.demo.urlshortener.dao;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * DAO MongoDB object to manage URL shortener
 */
@Document
public class URLShortener {
    @Id
    private String id;

    @Indexed
    private String url;

    //TODO : Batch/schedule to clean old/expire data
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    private Date updateDate;

    public URLShortener(String id, String url) {
        this.id = id;
        this.url = url;
        this.updateDate = new Date();
    }

    public String getId() {
        return id;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    @Override
    public String toString() {
        return "URLShortener{" +
                "id='" + id + '\'' +
                ", url='" + url + '\'' +
                ", updateDate=" + updateDate +
                '}';
    }
}
