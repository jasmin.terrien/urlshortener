package com.jtn.demo.urlshortener.repository;

import com.jtn.demo.urlshortener.dao.URLShortener;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

/**
 * MongoDB repository to manage URLShortener
 */
@Repository
public interface URLShortenerRepository extends MongoRepository<URLShortener, String> {
    URLShortener findByUrl(String Url);
}
