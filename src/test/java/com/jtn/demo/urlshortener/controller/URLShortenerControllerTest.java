package com.jtn.demo.urlshortener.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.jtn.demo.urlshortener.exception.URLGenerationErrorException;
import com.jtn.demo.urlshortener.exception.URLNotFoundException;
import com.jtn.demo.urlshortener.model.URLShortenerJSON;
import com.jtn.demo.urlshortener.service.URLShortenerService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.data.mongo.MongoDataAutoConfiguration;
import org.springframework.boot.autoconfigure.mongo.MongoAutoConfiguration;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringRunner.class)
@WebMvcTest(URLShortenerController.class)
@ActiveProfiles(profiles = "test")
public class URLShortenerControllerTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private URLShortenerService service;

    private static final String HOST_URL="http://localhost:80/r/";
    private static final String GOOD_URL="https://start.spring.io/";
    private static final String TEST_ID="aaaa";

    @Test
    public void URLToShorten_OK() throws Exception {

        URLShortenerJSON input = new URLShortenerJSON(GOOD_URL);

        Mockito.when(service.shortenURL(Mockito.any(String.class))).thenReturn(TEST_ID);

        mvc.perform(MockMvcRequestBuilders.post("/")
                .content(asJsonString(input))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.url").value(HOST_URL+TEST_ID));
    }

    @Test
    public void URLToShorten_KO_EMPTY() throws Exception {
        URLShortenerJSON input = new URLShortenerJSON("");

        mvc.perform(MockMvcRequestBuilders.post("/")
                .content(asJsonString(input))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void URLToShorten_KO_NO_URI() throws Exception {
        URLShortenerJSON input = new URLShortenerJSON("Pouet");

        mvc.perform(MockMvcRequestBuilders.post("/")
                .content(asJsonString(input))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void URLToShorten_KO_FTP() throws Exception {
        URLShortenerJSON input = new URLShortenerJSON("ftp://start.spring.io/");

        mvc.perform(MockMvcRequestBuilders.post("/")
                .content(asJsonString(input))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void URLToShorten_KO_XSS() throws Exception {

        URLShortenerJSON input = new URLShortenerJSON("http://testing.com/book.html?default=<script>alert(document.cookie)</script>");

        mvc.perform(MockMvcRequestBuilders.post("/")
                .content(asJsonString(input))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void URLToShorten_KO_NO_ID_AVAILABLE() throws Exception {

        URLShortenerJSON input = new URLShortenerJSON(GOOD_URL);

        Mockito.when(service.shortenURL(Mockito.any(String.class))).thenThrow(new URLGenerationErrorException());

        mvc.perform(MockMvcRequestBuilders.post("/")
                .content(asJsonString(input))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isUnprocessableEntity());
    }

    @Test
    public void ShortenToURL_OK() throws Exception {

        Mockito.when(service.getURLFromID(Mockito.any(String.class))).thenReturn(GOOD_URL);

        mvc.perform(MockMvcRequestBuilders.get("/r/"+TEST_ID))
                .andExpect(status().isFound())
                .andExpect(redirectedUrl(GOOD_URL));
    }

    @Test
    public void ShortenToURL_KO_NOT_GEN() throws Exception {

        Mockito.when(service.getURLFromID(Mockito.any(String.class))).thenThrow(new URLNotFoundException());

        mvc.perform(MockMvcRequestBuilders.get("/r/pouet"))
                .andExpect(status().isFound())
                .andExpect(redirectedUrl("../notfound"));
    }

    @Test
    public void ShortenToURL_KO_INPUT_TOO_LONG() throws Exception {

        Mockito.when(service.getURLFromID(Mockito.any(String.class))).thenThrow(new URLNotFoundException());

        mvc.perform(MockMvcRequestBuilders.get("/r/1234567891011"))
                .andExpect(status().isFound())
                .andExpect(redirectedUrl("../teapot"));
    }

    public static String asJsonString(final Object obj) {
        try {
            return new ObjectMapper().writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
