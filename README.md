# URL shortener

This project is a basic example of how to do a URL shortener

## How it works 

### Architecture
The project is based on Spring Boot 2 and use MongoDB for persistence.

There is two interfaces to generate the short URL :
- HTML web page
- REST JSON

### URL Shortener 'Algorithm'

If we take production needs as prerequisites, the solution must be able to manage a very large quantity of URLs (min: 100 millions) on rather fast response times with unique identifiers short and readable (<10 characters)

The entropy needed on the identifiers must therefore be quite important while keeping an optimized format and be reversible quickly. We can leave aside all cryptographic and non-cryptographic hash algorithms.

So we will use the technique used by the most known solutions (tinyurl, ...) which is to use tables of correspondences with a generation of id adapted to the need.

Whenever there is a new URL, we will generate a number in base 10 and encode it in base 62 so that the id remains readable and copiable humanly but compressed.

Unlike some and for simplicity, the identifier is not an incremented number but a random number over a large range (default: 5B62char => 62 ^ 5 => ~ 950 million).
This makes it possible to avoid collisions between parallel generations and non-predicability.


## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

- Maven
- Java 8+
- MongoDB

### Settings
You can change default configuration if needed.
Modify the config in the application-profile.properties :
```
#Business conf
shortener.maxlength=5
shortener.maxtry=5
#Mongodb Conf
spring.data.mongodb.host=localhost
spring.data.mongodb.port=27017
```

### Build

Using maven in the repository : 
```
mvn install
```


### Running
Basic command to launch your mongodb server and the spring boot server
```
#UNIX example
mongod --fork --logpath /var/log/mongod.log
java -jar urlshortener-0.0.1-SNAPSHOT.jar &>/dev/null &
```

After that, if you want to test easily, there is a integration script test
```
cd script
./integrationTest.sh
```
This will test the generation, redirect and error use case and do a little bench on 100 URL.
(See file .gitlab-ci)

### Usage : HTML
Go to your spring boot server (default dev : http://localhost:8080).

![alt text](img/WebExample.PNG "Web example")


### Usage : REST

You can use the REST API to generate the short url :

REQUEST : 
- HTTP Method : POST 
- Path "/"
- Body format : JSON
- Model : { "url" : "value"} 

```
curl -X POST \
  http://localhost:8080/ \
  -H 'Content-Type: application/json' \
  -d '{"url":"https://notarius.com/blogue/demythifier-le-%e2%80%afpiratage-des-signatures-pdf%e2%80%af/"}'
```
RESPONSE : 
- HTTP CODE : 200
- Body format : JSON
- Model : { "url" : "value"} 
```
{"url":"http://localhost:8080/r/sipD6"}
```

## TODO/Optimization

To have a more robust solution some improvement must be done : 
- Security enhancement : use only HTTPS and activate CSRF
- Bench on a massive amount of data
- Use BloomFilter (like Redis-Rebloom) to do very fast and light check of already existing URL or ID
- Batch/schedule/TTL to clean unused data

